export default function MobileToggle() {
    const $handle = $('#nav-toggle');
    const $nav = $('#main-nav');

    $handle.on('click touchstart', (event) => {
        event.preventDefault();
        $nav.fadeToggle();
        $handle.toggleClass('close');
    });
}
