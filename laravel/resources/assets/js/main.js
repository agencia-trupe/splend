import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners .cycle').cycle({
    slides: '>.slide',
});

const ps1 = new PerfectScrollbar('.scrollable-1');
const ps2 = new PerfectScrollbar('.scrollable-2');
const ps3 = new PerfectScrollbar('.scrollable-3');

$(window).on('resize', () => {
    ps1.update();
    ps2.update();
    ps3.update();
});

$('#form-contato').submit(function (event) {
    event.preventDefault();

    const $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: `${$('base').attr('href')}/contato`,
        data: {
            nome: $form.find('input[name=nome]').val(),
            email: $form.find('input[name=email]').val(),
            telefone: $form.find('input[name=telefone]').val(),
            mensagem: $form.find('textarea[name=mensagem]').val(),
        },
    })
        .done(() => {
            alert('Mensagem enviada com sucesso!');
            $form[0].reset();
        })
        .fail((data) => {
            if (data.status !== 422) {
                return;
            }

            const res = data.responseJSON;
            const txt = [];
            let field = '';

            for (field in res) {
                res[field].map(error => txt.push(error));
            }

            alert(txt.join('\n'));
        })
        .always(() => {
            $form.removeClass('sending');
        });
});

$('#main-nav .wrapper > a').click(function (event) {
    event.preventDefault();

    const id = $(this).attr('href');
    const $handle = $('#nav-toggle');
    const $nav = $('#main-nav');

    $handle.toggleClass('close');
    $nav.fadeOut();
    $('html, body').animate({
        scrollTop: $(id).offset().top,
    }, 'slow');
});

$(window).scroll(() => {
    const distanceFromTop = $(window).scrollTop();
    const sobreNosPos = $('.sobre-nos').position().top;
    const contatoPos = $('.contato').position().top;

    const showLogo = distanceFromTop >= sobreNosPos;
    const invertColors = distanceFromTop >= contatoPos;

    if (showLogo) {
        $('header').addClass('show-logo');
    } else {
        $('header').removeClass('show-logo');
    }

    if (invertColors) {
        $('header').addClass('invert');
    } else {
        $('header').removeClass('invert');
    }
});

$('.lang a').click(function (event) {
    event.preventDefault();

    const lightboxClass = $(this).attr('class');
    const $handle = $('#nav-toggle');
    const $nav = $('#main-nav');

    $('html, body').animate({
        scrollTop: 0,
    }, 'slow');

    $('.lightbox-overlay').addClass('visible');
    $(`.lightbox.${lightboxClass}`).addClass('visible');

    $handle.toggleClass('close');
    $nav.fadeOut();
});

$('.lightbox-overlay').click(function () {
    $('.lightbox').removeClass('visible');
    $(this).removeClass('visible');
});
