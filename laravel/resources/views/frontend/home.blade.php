@extends('frontend.common.template')

@section('content')

    <section class="banners">
        <div class="cycle">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
            @endforeach
        </div>
        <div class="content">
            <img src="{{ asset('assets/img/layout/marcca-splend.svg') }}" alt="">
            <p>
                SHOOTINGS INTERNACIONAIS NOS DESTINOS MAIS INCRÍVEIS
                <small>IMAGENS QUE IMPACTAM, FASCINAM E VENDEM</small>
            </p>
        </div>
    </section>

    <section class="sobre-nos" id="sobre-nos">
        <h1>SOBRE NÓS</h1>
        <div class="texto">{!! $sobreNos->texto !!}</div>
        <div class="imagens">
            @foreach(range(1, 4) as $i)
            <div>
                <img src="{{ asset('assets/img/sobre-nos/'.$sobreNos->{'imagem_'.$i}) }}" alt="">
            </div>
            @endforeach
        </div>
    </section>

    <section class="shooting" id="shooting">
        <h1>FAÇA SEU SHOOTING INTERNACIONAL</h1>
        <div class="textos">
            <div class="texto-1">{!! $shooting->texto_1 !!}</div>
            <div class="texto-2">{!! $shooting->texto_2 !!}</div>
        </div>
        <div class="imagem" style="background-image:url({{ asset('assets/img/shooting/'.$shooting->imagem) }})">
            @if($shooting->video)
            <div class="video">
                <div class="video-wrapper">
                    <iframe src="https://www.youtube.com/embed/{{ $shooting->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            @endif
        </div>
    </section>

    <section class="servicos" id="servicos">
        <h1>SERVIÇOS</h1>
        <div class="grid">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_1) }}" alt="">
            <div class="texto-1">
                <div class="scrollable scrollable-1">
                    {!! $servicos->texto_1 !!}
                </div>
            </div>
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_2) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_3) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_4) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_5) }}" alt="">
            <div class="texto-2">
                <div class="scrollable scrollable-2">
                    {!! $servicos->texto_2 !!}
                </div>
            </div>
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_6) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_7) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_8) }}" alt="">
            <img src="{{ asset('assets/img/servicos/'.$servicos->imagem_9) }}" alt="">
        </div>
    </section>

    <section class="relevancia" id="relevancia">
        <h1>RELEVÂNCIA</h1>
        <div class="imagem" style="background-image: url({{ asset('assets/img/relevancia/'.$relevancia->imagem) }})">
            <div class="texto">
                <div class="scrollable scrollable-3">
                    <div class="wrapper">
                        {!! $relevancia->texto !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="como-funciona" id="como-funciona">
        <h1>COMO FUNCIONA</h1>
        <div class="chamadas">
            @foreach(range(1, 5) as $i)
            <div class="chamada">
                <img src="{{ asset('assets/img/layout/icone-comofunciona'.$i.'.svg') }}" alt="">
                <p>{{ $comoFunciona->{'chamada_'.$i} }}</p>
            </div>
            @endforeach
        </div>
    </section>

    <section class="contato" id="contato">
        <h1>CONTATO</h1>
        <p>{{ $contato->telefone }}</p>
        <div class="wrapper">
            <form action="" id="form-contato">
                <div class="col">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" placeholder="telefone">
                </div>
                <textarea name="mensagem" placeholder="mensagem" required></textarea>
                <button type="submit"></button>
            </form>
            <div class="info">
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                @if($contato->instagram)
                <a href="https://www.instagram.com/{{ $contato->instagram }}" class="instagram" target="_blank">{{ '@'.$contato->instagram }}</a>
                @endif
            </div>
        </div>
        <div class="copyright">
            <p>
                &copy; {{ date('Y') }} {{ config('app.name') }} Todos os direitos reservados.
                <span>|</span>
                <a href="https://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </section>

@endsection
