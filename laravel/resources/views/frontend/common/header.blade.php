    <header>
        <div class="logo">{{ config('app.name') }}</div>
    </header>
    <div class="nav-wrapper">
        <button id="nav-toggle">
            <div class="lines"></div>
        </button>
        @if($contato->instagram)
        <a href="https://www.instagram.com/{{ $contato->instagram }}" class="instagram" target="_blank">
            {{ '@'.$contato->instagram }}
        </a>
        @endif
    </div>
    <nav id="main-nav">
        <div class="wrapper">
            <a href="#sobre-nos">SOBRE NÓS</a>
            <a href="#shooting">FAÇA SEU SHOOTING INTERNACIONAL</a>
            <a href="#servicos">NOSSOS SERVIÇOS</a>
            <a href="#relevancia">RELEVÂNCIA</a>
            <a href="#como-funciona">COMO FUNCIONA</a>
            <a href="#contato">CONTATO</a>
            <div class="lang">
                <a href="#" class="lightbox-en">[ENGLISH]</a>
                <a href="#" class="lightbox-es">[ESPAÑOL]</a>
            </div>
        </div>
    </nav>
    <div class="lightbox-overlay"></div>
    <div class="lightbox lightbox-en">
        <img src="{{ asset('assets/img/layout/marca-splend-lettering.svg') }}" alt="">
        {!! $sobreNos->texto_ingles !!}
    </div>
    <div class="lightbox lightbox-es">
        <img src="{{ asset('assets/img/layout/marca-splend-lettering.svg') }}" alt="">
        {!! $sobreNos->texto_espanhol !!}
    </div>
