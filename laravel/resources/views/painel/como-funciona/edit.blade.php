@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Como Funciona</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.como-funciona.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.como-funciona.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
