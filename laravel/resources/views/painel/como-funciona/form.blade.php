@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('chamada_1', 'Chamada 1') !!}
    {!! Form::text('chamada_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_2', 'Chamada 2') !!}
    {!! Form::text('chamada_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_3', 'Chamada 3') !!}
    {!! Form::text('chamada_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_4', 'Chamada 4') !!}
    {!! Form::text('chamada_4', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_5', 'Chamada 5') !!}
    {!! Form::text('chamada_5', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
