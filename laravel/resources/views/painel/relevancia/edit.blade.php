@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Relevância</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.relevancia.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.relevancia.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
