<ul class="nav navbar-nav">
	<li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
	<li @if(Tools::routeIs('painel.sobre-nos*')) class="active" @endif>
		<a href="{{ route('painel.sobre-nos.index') }}">Sobre Nós</a>
	</li>
	<li @if(Tools::routeIs('painel.shooting*')) class="active" @endif>
		<a href="{{ route('painel.shooting.index') }}">Shooting</a>
	</li>
	<li @if(Tools::routeIs('painel.servicos*')) class="active" @endif>
		<a href="{{ route('painel.servicos.index') }}">Serviços</a>
	</li>
	<li @if(Tools::routeIs('painel.relevancia*')) class="active" @endif>
		<a href="{{ route('painel.relevancia.index') }}">Relevância</a>
	</li>
	<li @if(Tools::routeIs('painel.como-funciona*')) class="active" @endif>
		<a href="{{ route('painel.como-funciona.index') }}">Como Funciona</a>
	</li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
