@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Shooting</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.shooting.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.shooting.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
