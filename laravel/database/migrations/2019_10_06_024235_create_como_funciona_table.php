<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComoFuncionaTable extends Migration
{
    public function up()
    {
        Schema::create('como_funciona', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chamada_1');
            $table->text('chamada_2');
            $table->text('chamada_3');
            $table->text('chamada_4');
            $table->text('chamada_5');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('como_funciona');
    }
}
