<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShootingTable extends Migration
{
    public function up()
    {
        Schema::create('shooting', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1');
            $table->text('texto_2');
            $table->string('imagem');
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('shooting');
    }
}
