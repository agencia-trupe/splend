<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreNosTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_nos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_ingles');
            $table->text('texto_espanhol');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre_nos');
    }
}
