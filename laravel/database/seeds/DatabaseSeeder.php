<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(SobreNosSeeder::class);
		$this->call(ShootingSeeder::class);
		$this->call(ServicosSeeder::class);
		$this->call(RelevanciaSeeder::class);
		$this->call(ComoFuncionaSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
