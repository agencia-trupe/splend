<?php

use Illuminate\Database\Seeder;

class SobreNosSeeder extends Seeder
{
    public function run()
    {
        DB::table('sobre_nos')->insert([
            'texto' => '',
            'texto_ingles' => '',
            'texto_espanhol' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
        ]);
    }
}
