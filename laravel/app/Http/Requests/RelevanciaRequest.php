<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RelevanciaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'imagem' => 'image',
        ];
    }
}
