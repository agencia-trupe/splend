<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ShootingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1' => 'required',
            'texto_2' => 'required',
            'imagem' => 'image',
            'video' => '',
        ];
    }
}
