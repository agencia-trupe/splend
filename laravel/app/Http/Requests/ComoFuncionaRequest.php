<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ComoFuncionaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_1' => 'required',
            'chamada_2' => 'required',
            'chamada_3' => 'required',
            'chamada_4' => 'required',
            'chamada_5' => 'required',
        ];
    }
}
