<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\SobreNos;
use App\Models\Shooting;
use App\Models\Servicos;
use App\Models\Relevancia;
use App\Models\ComoFunciona;
use App\Models\Contato;

class HomeController extends Controller
{
    public function index()
    {
        $banners      = Banner::ordenados()->get();
        $sobreNos     = SobreNos::first();
        $shooting     = Shooting::first();
        $servicos     = Servicos::first();
        $relevancia   = Relevancia::first();
        $comoFunciona = ComoFunciona::first();
        $contato      = Contato::first();

        return view('frontend.home', compact(
            'banners',
            'sobreNos',
            'shooting',
            'servicos',
            'relevancia',
            'comoFunciona',
            'contato'
        ));
    }
}
