<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index()
    {
        $registro = Servicos::first();

        return view('painel.servicos.edit', compact('registro'));
    }

    public function update(ServicosRequest $request, Servicos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Servicos::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Servicos::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Servicos::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Servicos::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = Servicos::upload_imagem_5();
            if (isset($input['imagem_6'])) $input['imagem_6'] = Servicos::upload_imagem_6();
            if (isset($input['imagem_7'])) $input['imagem_7'] = Servicos::upload_imagem_7();
            if (isset($input['imagem_8'])) $input['imagem_8'] = Servicos::upload_imagem_8();
            if (isset($input['imagem_9'])) $input['imagem_9'] = Servicos::upload_imagem_9();

            $registro->update($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
