<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComoFuncionaRequest;
use App\Http\Controllers\Controller;

use App\Models\ComoFunciona;

class ComoFuncionaController extends Controller
{
    public function index()
    {
        $registro = ComoFunciona::first();

        return view('painel.como-funciona.edit', compact('registro'));
    }

    public function update(ComoFuncionaRequest $request, ComoFunciona $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.como-funciona.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
