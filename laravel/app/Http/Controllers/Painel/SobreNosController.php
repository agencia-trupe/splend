<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreNosRequest;
use App\Http\Controllers\Controller;

use App\Models\SobreNos;

class SobreNosController extends Controller
{
    public function index()
    {
        $registro = SobreNos::first();

        return view('painel.sobre-nos.edit', compact('registro'));
    }

    public function update(SobreNosRequest $request, SobreNos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = SobreNos::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = SobreNos::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = SobreNos::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = SobreNos::upload_imagem_4();

            $registro->update($input);

            return redirect()->route('painel.sobre-nos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
