<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ShootingRequest;
use App\Http\Controllers\Controller;

use App\Models\Shooting;

class ShootingController extends Controller
{
    public function index()
    {
        $registro = Shooting::first();

        return view('painel.shooting.edit', compact('registro'));
    }

    public function update(ShootingRequest $request, Shooting $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Shooting::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.shooting.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
