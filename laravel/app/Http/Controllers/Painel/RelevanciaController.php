<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RelevanciaRequest;
use App\Http\Controllers\Controller;

use App\Models\Relevancia;

class RelevanciaController extends Controller
{
    public function index()
    {
        $registro = Relevancia::first();

        return view('painel.relevancia.edit', compact('registro'));
    }

    public function update(RelevanciaRequest $request, Relevancia $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Relevancia::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.relevancia.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
