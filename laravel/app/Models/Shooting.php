<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Shooting extends Model
{
    protected $table = 'shooting';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1980,
            'height' => 720,
            'path'   => 'assets/img/shooting/'
        ]);
    }

}
