<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_6()
    {
        return CropImage::make('imagem_6', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_7()
    {
        return CropImage::make('imagem_7', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_8()
    {
        return CropImage::make('imagem_8', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_imagem_9()
    {
        return CropImage::make('imagem_9', [
            'width'  => 500,
            'height' => 500,
            'path'   => 'assets/img/servicos/'
        ]);
    }

}
