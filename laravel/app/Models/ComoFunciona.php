<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ComoFunciona extends Model
{
    protected $table = 'como_funciona';

    protected $guarded = ['id'];

}
