<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SobreNos extends Model
{
    protected $table = 'sobre_nos';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 250,
            'height' => 250,
            'path'   => 'assets/img/sobre-nos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 250,
            'height' => 250,
            'path'   => 'assets/img/sobre-nos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 250,
            'height' => 250,
            'path'   => 'assets/img/sobre-nos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 250,
            'height' => 250,
            'path'   => 'assets/img/sobre-nos/'
        ]);
    }

}
